const {remote} = require('electron');
const experiment = remote.getGlobal("experiment")

const io = require('socket.io-client')
let peerConnection;

let cl, clId;
let sequenceClient;
const config = {
  iceServers: [
      {
        "urls": "stun:stun.l.google.com:19302",
      },
      // {
      //   "urls": "turn:TURN_IP?transport=tcp",
      //   "username": "TURN_USERNAME",
      //   "credential": "TURN_CREDENTIALS"
      // }
  ]
};

//const socket = io("http://137.226.117.197:3000");
const socket = io("https://mtlg.elearn.rwth-aachen.de", {path:"/orchestrator/socket.io"});
const video = document.querySelector("video");

const btnAuth = document.getElementById("btnAuth");
const btnFetchClients = document.getElementById("btnFetchClients");
const btnClientSelect = document.getElementById("btnClientSelected")
/*const b3 = document.getElementById("button3"); //request sources
const b4 = document.getElementById("button4"); // proceed
const b5 = document.getElementById("button5"); //start sequence
const b6 = document.getElementById("button6");  //start streaming*/
const l  = document.getElementById("log");
const clientSelect = document.getElementById("clientSelect");
const clientList = document.getElementById("clientList");
const sourcelist = document.getElementById("sourcelist");
const stageCont = document.getElementById("stagecontainer");

const room = "R6309"

let currentChannel =""

/*clientList.addEventListener('change',()=>{
  cl=clientList.options[clientList.selectedIndex].value
  log(clientList.options[clientList.selectedIndex].innerHTML+" ausgewählt")

})*/

function selectSequenceClient(){
  sequenceClient = clientList.options[clientList.selectedIndex].value
  parseSequence()
  sendSequence()
}

function parseSequence(){
  if(!experiment.hasOwnProperty("sequence")) return
  let seq = experiment.sequence
  const seqCont = document.getElementById("progressContainer")
  seqCont.innerHTML = ""
  stageCont.innerHTML = ""
  seq.forEach((stage,index)=>{
    seqCont.innerHTML += `<div class="stage" id="s${index+1}" style="z-index:${seq.length-index};"><div>${stage.id}</div>`
    let nexttext = ""
    switch (stage.endCondition){
      case 'servermessage':
        nexttext = 'Diese Stage wartet auf die Bestätigung des Versuchsleiters. Klicken Sie auf weiter um fortzufahren:<div class="button" onclick="proceed()">Weiter</div>'
        break
      case 'timeout':
        nexttext = `Diese Stage geht nach ${stage.endConditionParam}ms automatisch zur nächsten über.`
        break
      case 'content-trigger':
        nexttext = `Diese Stage geht inhaltsbasiert zur nächsten über, sobald das Schlüsselwort ${stage.endConditionParam} erkannt wird.`
        break
      case 'event-listener':
        nexttext = `Diese Stage wartet darauf, dass die geladene Seite die injizierte Funktion mtlgClientBridge.stageFinished() aufruft.`
        break

    }
    stageCont.innerHTML += `<div id="stage${index+1}" class="card stagecard" >
                <div class="card-content">
                    ${stage.id}<br><br>${stage.description}<br><br>${nexttext}
                </div>
            </div>
            <hr>`
  })
  seqCont.firstChild.classList.add("big")
  stageCont.firstChild.classList.add("now")
}

function log(text){
  l.innerHTML += text.toString() + "<br>"
}

socket.on('ping',()=>{
  socket.emit('pong',Date.now());
})


socket.on("offer", (id, description) => {
  peerConnection = new RTCPeerConnection(config);
  peerConnection
    .setRemoteDescription(description)
    .then(() => peerConnection.createAnswer())
    .then(sdp => peerConnection.setLocalDescription(sdp))
    .then(() => {
      socket.emit("answer", id, peerConnection.localDescription);
    }).catch((e)=>console.log("Something happened in offer handling",e));
  peerConnection.ontrack = event => {
    video.srcObject = event.streams[0];
  };
  peerConnection.onicecandidate = event => {
    if (event.candidate) {
      socket.emit("candidate", id, event.candidate);
    }
  };
});


socket.on("candidate", (id, candidate) => {
  peerConnection
    .addIceCandidate(new RTCIceCandidate(candidate))
    .catch(e => console.error(e));
});

socket.on("connect", () => {

});



btnAuth.onclick = ()=>{
  let creds= {
    user:'matthias',
    password:'secret123!'
  }
  socket.emit("authenticateResearchAssistant",creds,(res)=>{
    console.log(res)
    log("Result of authentication:"+res)
    if(res==="Success"){
      getClients()
      document.getElementById("authCard").style.setProperty("display","none")
      document.getElementById("configCard").style.setProperty("display","block")
      //Eigene Karte verstecken, nächste holen
      //
    }
  })

}

btnFetchClients.onclick = getClients
btnClientSelect.onclick = selectSequenceClient

let selectedType = 'video';
let activeSelector = document.getElementById('logSelect')

function showLog(){
  activeSelector.classList.remove('is-active')
  activeSelector = document.getElementById('logSelect')
  activeSelector.classList.add('is-active')
  document.getElementById("videopreview").style.setProperty("display","none")
  document.getElementById("sourcelist").style.setProperty("display","none")
  document.getElementById("log").style.setProperty("display","block")

}

function selectType(type,target){
  document.getElementById("videopreview").style.setProperty("display","block")
  document.getElementById("sourcelist").style.setProperty("display","block")
  document.getElementById("log").style.setProperty("display","none")
  console.log(target)
  activeSelector.classList.remove('is-active')
  target.classList.add('is-active')
  activeSelector = target
  selectedType = type
  requestSources()
}
function requestSources(){
  //selectedType=dtype.options[dtype.selectedIndex].value
  if(!clId) {
    sourcelist.innerHTML = "No Client selected"
    return
  }
  console.log("selected",selectedType)
  socket.emit("requestSources",cl,selectedType,(sources)=>{
    //streamsource.innerHTML="<option value='self'>Client window</option><option value='self_screen'>Screen with Client window</option>"
    console.log(selectedType)
    switch(selectedType){
      case "video":
        console.log("Hier!")
        sourcelist.innerHTML = ""
        break
      case "screen":
        sourcelist.innerHTML = `<a class="tag" onclick="requestStream('self_screen',this)"><i class="fas fa-square is-small"></i>&nbsp; Screen with Client Window</a>`
        break
      case "window":
        sourcelist.innerHTML = `<a class="tag" onclick="requestStream('self',this)"><i class="fas fa-square is-small"></i>&nbsp; Client Window</a>`
        break
    }
    sources.forEach((source)=>{
      sourcelist.innerHTML += `<a class="tag" onclick="requestStream('${source.id}',this)"><i class="fas fa-square is-small"></i>&nbsp; ${source.name}</a>`
     // console.log(client)
      //<a class="tag"><i class="fas fa-circle is-small has-text-danger"></i>&nbsp; Cam 1</a>
      //<a class="tag"><i class="fas fa-square is-small"></i>&nbsp; Cam 2</a>
      //streamsource.innerHTML += `<option value="${source.id}">${source.name}</option>`
    })
    console.log(sources)
  })
}

function proceed() {
  socket.emit("proceed",sequenceClient)
}

function sendSequence(){
  let seq = experiment.sequence
  socket.emit("sendSequenceToClient",sequenceClient,seq)
}

let oldBtn
async function requestStream(source,btn){
  if(oldBtn){
    oldBtn.classList.replace("fa-circle", "fa-square")
    oldBtn.classList.remove("has-text-danger")
  }
  oldBtn = btn.firstChild
  oldBtn.classList.replace("fa-square", "fa-circle")
  oldBtn.classList.add("has-text-danger")
  await changeChannel()
  socket.emit("requestStream",cl,source,selectedType)
  log("Requesting stream")
}

async function changeChannel(){
  let targetChannel = clId+room
  if(currentChannel===targetChannel) return
  //if(currentChannel!=="") await peerConnection.close()
  //delete peerConnection

  currentChannel = targetChannel
  console.log(targetChannel)
  socket.emit("joinBroadcastChannel",targetChannel);
  socket.emit("watcher")

  setTimeout(()=>{

    setTimeout(()=>{},1000)

  },1000);
}

socket.on("broadcaster", () => {
  socket.emit("watcher");
});

window.onunload = window.onbeforeunload = () => {
  socket.close();
  peerConnection.close();
};

function getClients(){
    socket.emit("getClients",room,(c)=>{
      console.log(c)
      log(c.length + " Clients available")
      clientList.innerHTML=""
      clientSelect.innerHTML=""
      if(c.length>0) cl = c[0].socket

      c.forEach((client)=>{
        console.log(client)
        clientSelect.innerHTML += `<a class="dropdown-item" onclick="activateClient('${client.socket}','${client.clientID}',this)">${client.clientID}</a>`
        clientList.innerHTML += `<option value="${client.socket}">${client.clientID}</option>`
      })
    })
}

let oldActive
function activateClient(socket,client,caller){
  if(oldActive) oldActive.classList.remove('is-active')
  caller.classList.add('is-active')
  oldActive = caller
  cl = socket
  clId = client
  requestSources()
}

