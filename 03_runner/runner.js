
function toggle_preview(){
    const sl = document.getElementById('sourcelist')
    const vp = document.getElementById('videopreview')
    sl.hidden = !sl.hidden
    vp.hidden = !vp.hidden
    toggleIcon("media_toggleicon",!sl.hidden)
}
const tm = document.getElementById('toggle_media')
tm.onclick = toggle_preview

//const s2 = document.getElementById('s2')
//s2.onclick = stageLink

const stageLinks = document.getElementsByClassName("stage")
Array.from(stageLinks).forEach((e)=>e.onclick = stageLink)

function stageLink(e){
    //console.log(e.target.id.substr(1))
    const sc = document.getElementById("stagecontainer")
    const ts = document.getElementById("stage"+e.currentTarget.id.substr(1))
    console.log("e",e.currentTarget)
    console.log("stage"+e.currentTarget.id.substr(1))
    sc.scrollTop = ts.offsetTop
}


function toggleIcon(selector, state) {
    const arrow = {true:"fa-angle-up",false:"fa-angle-down"}
    let elem = document.getElementById(selector);
    console.log("Toggle")
    elem.classList = null;
    elem.classList.add("fas");
    elem.classList.add(arrow[state]);
}

