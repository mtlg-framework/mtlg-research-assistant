const {remote, ipcRenderer} = require('electron');
const {prompt} = require('../helpers/prompt')

const experiment = remote.getGlobal("experiment")

const titleBtn = document.getElementById("titleBtn")
const modeBtn = document.getElementById("modeBtn")
const setupBtn = document.getElementById("setupBtn")
const sessionBtn = document.getElementById("sessionBtn")
const sequenceBtn = document.getElementById("sequenceBtn")
const metaBtn = document.getElementById("metaBtn")
const dataBtn = document.getElementById("dataBtn")

const backBtn = document.getElementById("backButton")

const expTitle = document.getElementById("expTitle")
const expMode = document.getElementById("expMode")
const expSetup = document.getElementById("expSetup")
const expSession = document.getElementById("expSession")
const expSequence = document.getElementById("expSequence")
const expMeta = document.getElementById("expMeta")
const expData = document.getElementById("expData")

expTitle.innerText = experiment.title
expMode.innerText = experiment.mode
expSetup.innerText = experiment.orchestrator
expSession.innerText = (experiment.hasOwnProperty('session'))?experiment.session.description:"undefined"
expSequence.innerText = (experiment.hasOwnProperty('sequence'))?"ready":"undefined"
expMeta.innerText = (experiment.hasOwnProperty('meta'))?"Details set":"undefined"
expData.innerText = (experiment.hasOwnProperty('data'))?experiment.data.samplesSize+" Sets collected":"Nothing collected yet"

titleBtn.onclick = updateTitle
modeBtn.onclick = updateMode
setupBtn.onclick = updateSetup

sequenceBtn.onclick = updateSequence

backBtn.onclick = () => ipcRenderer.send("toStart")

function updateTitle() {
    const dialogContent = `Bitte den Experimenttitel eingeben:<br><input class="input" type="text" id="newTitle" value="${experiment.title}">`
    prompt.show(dialogContent, () => {
        expTitle.innerText = document.getElementById("newTitle").value
        updateExperiment("title", document.getElementById("newTitle").value)
    })
}

function updateSetup() {
    const dialogContent = `Bitte die Adresse des Orchestrators eingeben:<br><input class="input" type="text" id="newOrchestrator" value="${experiment.orchestrator}">`
    prompt.show(dialogContent, () => {
        expSetup.innerText = document.getElementById("newOrchestrator").value
        updateExperiment("orchestrator", document.getElementById("newOrchestrator").value)
    })
}

function updateMode() {
    const dialogContent = `Bitte den Experimenttyp auswählen:<br><div class="select">
  <select id="mode">
    <option value="Supervised Group">Gruppe, beaufsichtigt</option>
    <option value="Unsupervised Group">Gruppe, autonom</option>
    <option value="Supervised Individual">Einzeln, beaufsichtigt</option>
    <option value="Unsupervised Individual">Einzeln, autonom, online</option>
    <option value="Unsupervised Individual Offline">Einzeln, autonom, offline</option>
  </select>
</div>`
    alert("oh oh")
    prompt.show(dialogContent, () => {
        expMode.innerText = document.getElementById("mode").value
        updateExperiment("mode", document.getElementById("mode").value)
    })
}

function updateSequence() {
    ipcRenderer.send("updateSequence", experiment)
}

function runSequence() {
    ipcRenderer.send("runSequence", experiment)
}

(() => {
    prompt.init()
    if(experiment.hasOwnProperty('sequence') && experiment.hasOwnProperty('mode') && experiment.mode.substr(0,10)=="Supervised"){
        document.getElementById("startbtn").disabled = false
        document.getElementById("startbtn").onclick = runSequence
    }
})()

function updateExperiment(key, value) {
    experiment[key] = value
    console.log("experiment attribute", key, "changed to", value)
    ipcRenderer.send("updateExperiment", experiment)
}

