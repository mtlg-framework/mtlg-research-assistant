const {remote, ipcRenderer} = require('electron');

const experiment = remote.getGlobal("experiment")

const backBtn = document.getElementById("backButton")
const saveBtn = document.getElementById("saveBtn")
const sequenceTable = document.getElementById("sequenceTable")
const editTable = document.getElementById("editTable")

const stageName = document.getElementById("stageName")
const stageType = document.getElementById("stageType")
const stageSource = document.getElementById("stageSource")
const stageCondition = document.getElementById("stageCondition")
const stageParam = document.getElementById("stageParam")

const enumTypes = {"url":0,"msg":1,"package":2,"file":3}
const enumConds = {"servermessage":0,"timeout":1,"content-trigger":2,"event-listener":3}
let addBtn, inEdit, seq
backBtn.onclick = () => ipcRenderer.send("toOverview")
saveBtn.onclick = save


function insertRow(){
    const newStage = {id:'New Stage'}

//    if(!experiment.hasOwnProperty('sequence')) experiment.sequence=[]
    seq.push(newStage)
    renderTable()
}

function edit(index){
    inEdit = index
    editTable.style.setProperty("display","block")
    stageName.value = seq[index].id
    stageType.selectedIndex = enumTypes[seq[index].type || "msg"]
    stageSource.value = seq[index].source || "no no"
    stageCondition.selectedIndex = enumConds[seq[index].endCondition || "servermessage"]
    stageParam.value = seq[index].endConditionParam || ""
}

function save(){
    seq[inEdit].id=stageName.value
    seq[inEdit].type = stageType.value
    seq[inEdit].source = stageSource.value
    seq[inEdit].endCondition = stageCondition.value
    seq[inEdit].endConditionParam = stageParam.value
    updateExperiment("sequence",seq)
    renderTable()
    inEdit = undefined
    editTable.style.setProperty("display","none")
}

function renderTable(){
    sequenceTable.innerHTML= `<tr>
                    <td>Sequenzschritt</td>
                    <td></td>
                </tr>
                <tr id="lastLine">
                    <td>Schritt hinzufügen</td>
                    <td><i class="fas fa-plus" id="addBtn"></i></td>
                </tr>`
    const lastLine =  document.getElementById("lastLine")
    addBtn = document.getElementById("addBtn")
    addBtn.onclick = insertRow
    seq.forEach((stage, i) => {
        const row = document.createElement("tr")
        row.innerHTML = `<td>${stage.id}</td><td> ${(i>0)?'<i class="fas fa-angle-up" onclick="up('+i+')"></i>':''} ${(i<(seq.length-1))?'<i class="fas fa-angle-down" onclick="down('+i+')"></i>':''}</i> <i class="fas fa-edit" onclick="edit(${i})"></i></td>`
        sequenceTable.lastChild.insertBefore(row,lastLine);
    })
}

function updateExperiment(key, value) {
    experiment[key] = value
    console.log("experiment attribute", key, "changed to", value)
    ipcRenderer.send("updateExperiment", experiment)
}

function up(index){
    const tmp = seq[index-1]
    seq[index-1]=seq[index]
    seq[index]=tmp
    updateExperiment("sequence",seq)
    renderTable()
}

function down(index){
    const tmp = seq[index+1]
    seq[index+1]=seq[index]
    seq[index]=tmp
    updateExperiment("sequence",seq)
    renderTable()
}

(()=>{
    seq = []
    if (experiment.hasOwnProperty('sequence')) {
        seq = experiment.sequence
    }
    renderTable()
})();
