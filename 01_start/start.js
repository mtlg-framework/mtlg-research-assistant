const { remote, ipcRenderer } = require('electron');
const { dialog } = remote;
const fs = require('fs')


const histbtn = document.getElementById("loadhistbtn")
const newbtn = document.getElementById("newbtn")
const loadbtn = document.getElementById("loadbtn")

let historySelect
let listitems

console.log(listitems)

function historyClick(){
    histbtn.disabled = false
    listitems.forEach(e=>e.classList.remove('selected'))
    this.classList.add('selected')
    console.log(this.lastChild.innerText)
    historySelect = this.lastChild.innerText
}


newbtn.onclick = handleNew
loadbtn.onclick = handleOpen
histbtn.onclick = handleHistory

async function handleNew(){
    const newExperiment = {title:'Untitled Experiment'}
    const { filePath } = await dialog.showSaveDialog({
        buttonLabel: 'Speichern',
        defaultPath: `experiment.mte`
    });
    fs.writeFileSync(filePath,JSON.stringify(newExperiment))
    console.log(filePath)
    proceed(filePath)
}

function handleHistory(){
    proceed(historySelect)
}

async function handleOpen(){
    const filePath  = await dialog.showOpenDialog({
        properties: ['openFile'],
        filters: [{name:'Multi-Touch Experiment Definitions',extensions:['mte']}]
    });
    if(!filePath.canceled){
        console.log(filePath.filePaths[0])
    }
    console.log(filePath)
    proceed(filePath.filePaths[0])
}

function proceed(fp){
    console.log("Filepath received",fp)
    const res = fs.existsSync(fp)
    if(!res) alert("Datei ist nicht (mehr) vorhanden")
    else   ipcRenderer.send('filepath', fp)
    console.log(res)
}

(()=>{
   const history=remote.getGlobal('history');
   let listElements=""
   history.forEach((h)=>{
       listElements += `<li class="exp"><b>${h.title}</b><br><span class="path">${h.path}</span></li>`
   })
    document.getElementById("ul").innerHTML=listElements

    listitems = Array.from(document.getElementsByClassName("exp"))
    listitems.forEach(e=>e.addEventListener('click',historyClick))
})()
