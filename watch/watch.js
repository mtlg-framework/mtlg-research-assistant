const io = require('socket.io-client')
let peerConnection;

let cl;
const config = {
  iceServers: [
      {
        "urls": "stun:stun.l.google.com:19302",
      },
      // {
      //   "urls": "turn:TURN_IP?transport=tcp",
      //   "username": "TURN_USERNAME",
      //   "credential": "TURN_CREDENTIALS"
      // }
  ]
};

//const socket = io("http://137.226.117.197:3000");
const socket = io("https://mtlg.elearn.rwth-aachen.de", {path:"/orchestrator/socket.io"});
const video = document.querySelector("video");

const b1 = document.getElementById("button1");
const b2 = document.getElementById("button2");
const b3 = document.getElementById("button3");
const b4 = document.getElementById("button4");
const b5 = document.getElementById("button5");
const b6 = document.getElementById("button6");
const l  = document.getElementById("log");
const clientList = document.getElementById("clientList");
const dtype = document.getElementById("devicetype");
const streamsource = document.getElementById("source");

const room = "R6309"

let currentChannel =""

clientList.addEventListener('change',()=>{
  cl=clientList.options[clientList.selectedIndex].value
  log(clientList.options[clientList.selectedIndex].innerHTML+" ausgewählt")

})


function log(text){
  l.innerHTML += text.toString() + "<br>"
}

socket.on('ping',()=>{
  socket.emit('pong',Date.now());
})


socket.on("offer", (id, description) => {
  peerConnection = new RTCPeerConnection(config);
  peerConnection
    .setRemoteDescription(description)
    .then(() => peerConnection.createAnswer())
    .then(sdp => peerConnection.setLocalDescription(sdp))
    .then(() => {
      socket.emit("answer", id, peerConnection.localDescription);
    }).catch((e)=>console.log("Something happened in offer handling",e));
  peerConnection.ontrack = event => {
    video.srcObject = event.streams[0];
  };
  peerConnection.onicecandidate = event => {
    if (event.candidate) {
      socket.emit("candidate", id, event.candidate);
    }
  };
});


socket.on("candidate", (id, candidate) => {
  peerConnection
    .addIceCandidate(new RTCIceCandidate(candidate))
    .catch(e => console.error(e));
});

socket.on("connect", () => {

});

b1.onclick = ()=>{
  socket.emit("getClients",room,(c)=>{
    console.log(c)
    log(c.length + " Clients available")
    clientList.innerHTML=""
    if(c.length>0) cl = c[0].socket

    c.forEach((client)=>{
      console.log(client)
      clientList.innerHTML += `<option value="${client.socket}">${client.clientID}</option>`
    })
  })
}

b2.onclick = ()=>{
  let creds= {
    user:'matthias',
    password:'secret123!'
  }
  socket.emit("authenticateResearchAssistant",creds,(res)=>{
    console.log(res)
    log("Result of authentication:"+res)
  })

}

let selectedType;
b3.onclick = ()=>{
  selectedType=dtype.options[dtype.selectedIndex].value
  socket.emit("requestSources",cl,dtype.options[dtype.selectedIndex].value,(sources)=>{
    streamsource.innerHTML="<option value='self'>Client window</option><option value='self_screen'>Screen with Client window</option>"
    sources.forEach((source)=>{
     // console.log(client)

      streamsource.innerHTML += `<option value="${source.id}">${source.name}</option>`
    })
    console.log(sources)
  })
}

b4.onclick = ()=>{
  socket.emit("proceed",cl)
}

b5.onclick = ()=>{
  let seq = demoSequence
  socket.emit("sendSequenceToClient",cl,seq)
}

b6.onclick = async ()=>{
  await changeChannel()
  socket.emit("requestStream",cl,streamsource.options[streamsource.selectedIndex].value,selectedType)
  log("Requesting stream")
}

async function changeChannel(){
  targetChannel = clientList.options[clientList.selectedIndex].innerHTML+room
  if(currentChannel===targetChannel) return
  //if(currentChannel!=="") await peerConnection.close()
  //delete peerConnection
  console.log(targetChannel)
  currentChannel = targetChannel
  socket.emit("joinBroadcastChannel",targetChannel);
  socket.emit("watcher")

  setTimeout(()=>{

    setTimeout(()=>{},1000)

  },1000);
}

socket.on("broadcaster", () => {
  socket.emit("watcher");
});

window.onunload = window.onbeforeunload = () => {
  socket.close();
  peerConnection.close();
};





let demoSequence = [
  {
    id: 'Google Calibration',
    type: 'url',                        //alt: file, package, localConfig (takes gamePackage (full filepath) from clientConfig, resorts to clientConfig.gameURL if empty)
    source: 'http://google.de',         //path to file in fs, name of (preimported) package, empty for localConfig  todo: list of packages provider
    injectables:{localconfig:{abc:'def',ghi:'jkl'}},                     //given object will be made available via window.mtlgClient
    endCondition: 'content-trigger',            //timeout ,alt: servermessage (socket.io on'nextStage'), content-trigger (webcontent/dom searchable?), event-listener (inject after loading?)
    endConditionParam: 'Salamipizza',          //timeout: millis, content-trigger: string, event listener: event name
    transition: 'immediate',            //alt: onSignal, todo: implement waiting message
    allowBackgroundTransmission:false,  //might be used in the future todo: think about handling data transfer after session (avoid closing window)
  },
  {
    id: 'Bing Calibration',
    type: 'url',                        //alt: file or package
    source: 'http://bing.de',         //path to file, name of package, todo: list of packages provider
    injectables:{autoCorrect: true, sossen:['mayo','joppi']},                     //given object will be made available via window.mtlgClient
    endCondition: 'event-listener',            //timeout ,alt: servermessage (socket.io on'nextStage'), content-trigger (webcontent/dom searchable?), event-listener (inject after loading?)
    endConditionParam: false,          //timeout: millis, content-trigger: string, event listener: false
    transition: 'immediate',            //alt: onSignal, todo: implement waiting message
    allowBackgroundTransmission:false,  //might be used in the future todo: think about handling data transfer after session (avoid closing window)
  },
  {
    id: 'Cat image',
    type: 'url',                        //alt: file or package
    source: 'https://www.myself.de/Undefined/image-thumb__8934__header/katzenfotos_1.jpeg',         //path to file, name of package, todo: list of packages provider
    injectables:{'belag':'chickenuggets'},                     //given object will be made available via window.mtlgClient
    endCondition: 'timeout',            //timeout ,alt: servermessage (socket.io on'nextStage'), content-trigger (webcontent/dom searchable?), event-listener (inject after loading?)
    endConditionParam: '5000',          //timeout: millis, content-trigger: string, event listener: event name
    transition: 'immediate',            //alt: onSignal, todo: implement waiting message
    allowBackgroundTransmission:false,  //might be used in the future todo: think about handling data transfer after session (avoid closing window)
  },
  {
    id: 'Cat image',
    type: 'msg',                        //alt: file or package
    source: 'Demosequence completed',         //path to file, name of package, todo: list of packages provider
    injectables:{},                     //given object will be made available via window.mtlgClient
    endCondition: 'event-listener',            //timeout ,alt: servermessage (socket.io on'nextStage'), content-trigger (webcontent/dom searchable?), event-listener (inject after loading?)
    endConditionParam: false,          //timeout: millis, content-trigger: string, event listener: event name
    transition: 'immediate',            //alt: onSignal, todo: implement waiting message
    allowBackgroundTransmission:false,  //might be used in the future todo: think about handling data transfer after session (avoid closing window)
  }
]
