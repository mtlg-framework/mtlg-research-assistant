const { app, BrowserWindow, ipcMain, Menu } = require('electron');
const path = require('path');
const Store = require('electron-store')
const fs = require('fs')

//copied from client from here
let clientConfig = undefined;

let experiment = {}
let currentFilePath = ""

const store = new Store();
let fileHistory = []

if (!(fileHistory = store.get('history'))) {
  fileHistory = []
}

global['history'] = fileHistory

function updateClientConfig(key, value) {
  clientConfig[key] = value;
  store.set('config', clientConfig);
}
//until here

// Handle creating/removing shortcuts on Windows when installing/uninstalling.
if (require('electron-squirrel-startup')) { // eslint-disable-line global-require
  app.quit();
}
let mainWindow
const createWindow = () => {
  // Create the browser window.
  mainWindow = new BrowserWindow({
    width: 1280,
    height: 960,
    webPreferences: {
      nodeIntegration: true,
      enableRemoteModule: true

    },
    resizable:false
  });
  drawMenu()
  // and load the index.html of the app.
  mainWindow.loadFile(path.join(__dirname, '../01_start/start.html'));
 //  mainWindow.loadFile(path.join(__dirname, '../watch/index.html'));
  // mainWindow.loadFile(path.join(__dirname, '../03_runner/runner.html'));

  // Open the DevTools.
  //mainWindow.webContents.openDevTools();
};

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow);

// Quit when all windows are closed.
app.on('window-all-closed', () => {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', () => {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (BrowserWindow.getAllWindows().length === 0) {
    createWindow();
  }
});

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and import them here.
ipcMain.on('filepath', (event, arg) => {
  //load exp from file
  experiment = JSON.parse(fs.readFileSync(arg))
  global["experiment"]=experiment
  mainWindow.loadFile(path.join(__dirname, '../02_planning/overview.html'));
  let pos=-1;
  fileHistory.forEach((h,i)=>{
    if(h.path==arg) pos = i
  })
  if(pos>=0) fileHistory.splice(pos,1)
  fileHistory.unshift({title:experiment.title,path:arg})
  fileHistory.splice(5)
  store.set('history',fileHistory)
  currentFilePath = arg
})

ipcMain.on('updateExperiment',(event, arg)=>{
  experiment=arg
  global["experiment"]=experiment
  fs.writeFileSync(currentFilePath,JSON.stringify(experiment))
  if(arg.hasOwnProperty("title")){
    fileHistory[0].title = experiment.title
    store.set('history',fileHistory)
  }
})
ipcMain.on('toStart',(event)=>{
  mainWindow.loadFile(path.join(__dirname, '../01_start/start.html'));
})
ipcMain.on('toOverview',(event)=>{
  mainWindow.loadFile(path.join(__dirname, '../02_planning/overview.html'));
})
ipcMain.on('updateSequence',(event)=>{
  mainWindow.loadFile(path.join(__dirname, '../02_planning/sequencer.html'));
})
ipcMain.on('runSequence',(event)=>{
  mainWindow.loadFile(path.join(__dirname, '../03_runner/runner.html'));
})

function drawMenu(){
  const template = [
    {
      label: 'Datei',
      submenu: [
        {
          label: 'Planung',
          click(){
            mainWindow.loadFile(path.join(__dirname, '../01_start/start.html'));
            //mainWindow.loadFile(path.join(__dirname, '../watch/index.html'));
            // mainWindow.loadFile(path.join(__dirname, '../runner/runner.html'));
          }
        },
        {
          label: 'Durchführung',
          click(){
            // mainWindow.loadFile(path.join(__dirname, '../start/start.html'));
            //mainWindow.loadFile(path.join(__dirname, '../watch/index.html'));
            mainWindow.loadFile(path.join(__dirname, '../03_runner/runner.html'));
          }        },
        {
          label: 'Techdemo',
          click(){
            // mainWindow.loadFile(path.join(__dirname, '../start/start.html'));
            mainWindow.loadFile(path.join(__dirname, '../watch/index.html'));
            // mainWindow.loadFile(path.join(__dirname, '../runner/runner.html'));
          }        }
      ]
    },
    {
      label: 'Debug',
      click(item, win) {
        win.webContents.toggleDevTools()
      }
    }
  ]
  const menu = Menu.buildFromTemplate(template)
  Menu.setApplicationMenu(menu);
}
