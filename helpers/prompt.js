
function closeModal() {
    document.getElementById("inputModal").classList.remove("is-active")
}

function show(modalContent, callback) {
    document.getElementById("inputModal").classList.add("is-active")
    document.getElementById("prompt-content").innerHTML = modalContent
    document.getElementById("save").onclick = () => {
        callback();
        closeModal();
    }
}

function init(){
    const modalContainer = document.createElement("div")
    modalContainer.id = "inputModal"
    modalContainer.classList.add("modal")
    const modalHTML = `<div class="modal-background" id="modal-bg"></div>
        <div class="modal-content">
            <div id="prompt-content"></div>
            <button id="cancel" class="button is-pulled-right">Abbrechen</button>
            <button id="save" class="button is-pulled-right is-primary">Speichern</button>
        </div>
        <button class="modal-close is-large" aria-label="close" id="close"></button>`
    modalContainer.innerHTML = modalHTML
    document.body.appendChild(modalContainer)

 //   Array.from(document.getElementsByClassName("modal-close")).forEach(m => m.onclick = closeModal)
    document.getElementById("close").onclick = closeModal
  //  Array.from(document.getElementsByClassName("modal-background")).forEach(m => m.onclick = closeModal)
    document.getElementById("modal-bg").onclick = closeModal
    document.getElementById("cancel").onclick = closeModal
}
const prompt ={show, init}
exports.prompt = prompt
